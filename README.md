# TableauLong

Converts wide data to a long format appropriate for Tableau.

## Installation
~~~
    net from https://gitlab.com/tpayne9/tableaulong/-/raw/master/

    net install tableaulong
    
    help tableaulong

~~~
## What is this thing?
You have data like:

| id     | num_assaults | num_robberies |
| ------ | ------       | ------        |
| 1      | 12           | 34            |
| 2      | 56           | 78            |

but you want data like:

| id | measure      | value |
|--- | ------       |------ |
| 1 | num_assaults  | 12    |
| 1 | num_robberies | 34    |
| 2 | num_assaults  | 56    |
| 2 | num_robberies | 78    |

tableaulong does this for you, saving the result as a CSV that can be imported to Tableau easily.  It also saves the measure's variable label and the value labels.
