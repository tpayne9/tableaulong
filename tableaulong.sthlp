{smcl}
{* *! version 0.1  11aug2020}{...}

{title:Title}

{phang}
{bf:tableaulong} {hline 2} Create Tableau-friendly long CSV


{marker syntax}{...}
{title:Syntax}

{p 8 17 2}
{cmdab:tableaulong using}
{it:filename} 
, {cmdab:i:d(varname)} {cmdab:v:ariables(varlist)} [replace]


{synoptset 20 tabbed}{...}
{synopthdr}
{synoptline}
{syntab:Main}
{synopt:{it:filename}} output filename. {p_end}
{synopt:{cmdab:i:d(varname)}} variable that uniquely identifies observations (rows) in the
data.

{synopt:{cmdab:v:ariables(varlist)}} Stata variable list of measures to include in the output file.
Must be numeric. {p_end}

{synopt:{it:replace}} optional.  Replaces output file, if it already exists. {p_end}

{marker description}{...}
{title:Description}

{pstd}
{cmd:tableaulong} creates a long dataset in CSV format that is suitable for 
import into Tableau for visualization.

{marker remarks}{...}
{title:Remarks}

{pstd}
Tableau prefers data in long format.  More than that, however, it is easier to work with 
data that is in key-value pairs, where each record is a unique combination of the unit of
analysis, measure name, and measure value.  This is different from Stata's long format.  {p_end}

{pstd}
Stata's {cmd:reshape} command can translate between these structures:

           {it:long}
        {c TLC}{hline 12}{c TRC}                  {it:wide}
        {c |} {it:i  j}  {it:stub} {c |}                 {c TLC}{hline 16}{c TRC}
        {c |}{hline 12}{c |}                 {c |} {it:i}  {it:stub}{bf:1} {it:stub}{bf:2} {c |}
        {c |} 1  {bf:1}   4.1 {c |}     reshape     {c |}{hline 16}{c |}
        {c |} 1  {bf:2}   4.5 {c |}   <{hline 9}>   {c |} 1    4.1   4.5 {c |}
        {c |} 2  {bf:1}   3.3 {c |}                 {c |} 2    3.3   3.0 {c |}
        {c |} 2  {bf:2}   3.0 {c |}                 {c BLC}{hline 16}{c BRC}
        {c BLC}{hline 12}{c BRC}  
        
{pstd}{cmd:tableaulong} will create this structure from wide data:

  {it:        tableaulong}
        {c TLC}{hline 20}{c TRC}              
        {c |} {it:id  measure}  {it:value} {c |}     
        {c |}{hline 20}{c |}                 
        {c |} 1  {bf: stub1}     4.1  {c |}     
        {c |} 1  {bf: stub2}     4.5  {c |}   
        {c |} 2  {bf: stub1}     3.3  {c |}   
        {c |} 2  {bf: stub2}     3.0  {c |}   
        {c BLC}{hline 20}{c BRC}  

{pstd} 
{cmdab:tableaulong} does not care if the variables share a common {bf:stub}.  
Variable labels and value labels (when present) are also written out to the file.{p_end}

{marker notes}{...}
{title:Notes}
{pstd}
{cmd:tableaulong} can produce {it:very} long files.  The output CSV will be 5 variables 
wide, and will contain _N * the number of variables in {cmdab:v:ariables(varlist)}.
{p_end}

{marker Author}{...}
{title:Author}
{pstd}
Troy Payne {p_end}
{pstd}
Alaska Justice Information Center {p_end}
{pstd}
University of Alaska Anchorage {p_end}
{pstd}
tpayne9@alaska.edu {p_end}

