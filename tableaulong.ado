*! 0.1.2 TCP 13aug2020


cap program drop tableaulong

program tableaulong 
version 16.1
syntax using , Id(varname) Variables(varlist numeric) [replace]
cap isid `id'
if _rc {
	di as error "ID variable does not uniquely identify observations"
	exit
}

local type_id: type `id'
local display_id: format `id'

local v_count: word count `variables'

local totalobs = `v_count' * _N
di as text "This will produce a file with " as result "5 variables" as text " and " as result "`totalobs' observations."

tempname typeframe resultframe
mkf `typeframe' str10 v_type

// get variable types
foreach v of varlist `variables' {
	local v_type: type `v'
	frame post `typeframe' ("`v_type'")
	
	
}

// detect largest type
frame `typeframe': quietly levelsof v_type, clean
if regexm("`r(levels)'", "(str[12][0-9]?[0-9]?[0-9]?)") {
	local r_type = regexs(0)
}
else if strmatch("`r(levels)'", "*double*") local r_type  "double"
else if strmatch("`r(levels)'", "*float*") local r_type  "float"
else if strmatch("`r(levels)'", "*long*") local r_type  "long"
else if strmatch("`r(levels)'", "*int*") local r_type  "int"
else local r_type  "byte"

// detect measure name measure label and value label maxlength
local nvars: word count `variables'
tokenize `variables'
local m_len: strlen local 1
local varl_len: strlen local 1


local vallab_len: strlen local 1

forval i = 2/`nvars' {
	local i_len:  strlen local `i'
	if `i_len' > `m_len' local m_len =`i_len'
	
	local varlabel: variable label ``i''
	local il_len: strlen local varlabel
	if `il_len' > `varl_len' local varl_len = `il_len'
	
	local vallabname: value label ``i''
	if "`vallabname'" != "" {
		local i_vallabellen: label `vallabname' maxlength
		
		if `i_vallabellen' != 0 {
			if `i_vallabellen' > `vallab_len' local vallab_len = `i_vallabellen'
		}
	}
}
	
// create result frame
mkf `resultframe' `type_id' `id' str`m_len' measure str`varl_len' measure_label `r_type' value str`vallab_len' value_label


local n_obs = _N

forval var = 1/`nvars' {
	
	di "Variable ``var''"
	di "  Progress:      "
	di "   [" _continue
	local tenpercent = floor(`n_obs' / 10)
	
	forval obs = 1/`n_obs' {
		if mod(`obs', `tenpercent') == 0 di "." _continue
		local varlabel: variable label ``var''
		local vallabname: value label ``var''
		local val = ``var''[`obs']
		if "`vallabname'" != "" local vallabval: label `vallabname' `val', strict
		frame post `resultframe' (`id'[`obs']) ("``var''") ("`varlabel'") (``var''[`obs']) ("`vallabval'")
	}
	di "]" _newline
}
frame `resultframe': format `id' `display_id' 
frame `resultframe': export delimited `using' , `replace' datafmt 
end


/*eof
local vallabelname: value label foreign
di "`vallabelname'"
local valname: label `vallabelname' maxlength
di "`valname'"
qui {
di "Variable ``var''"
	noi di "Progress:      "
	noi di "   [" _continue
	noi di "."
}

clear
input str2 id float(victim services)
"1"  0 .
"2"  1 0
"3"  0 .
"4"  1 1
"5"  0 .
"6"  1 0
"7"  0 .
"8"  1 1
"9"  0 .
"10" 1 0
end
